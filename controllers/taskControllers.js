const Task = require('../models/task')


//Getting all data
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}



//Creating a task
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error)=> {
		if(error){
			console.log(error)
			return false;
		}else{
			return task
		}
	})
}

//Deleting a Task
module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err)
			return false
		}else{
			return removedTask
		}
	})
}

//Updating a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err){
			console.log(err)
			return false
		}
		result.name = newContent.name
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr)
				return false
			} else{
				return updatedTask
			}
		})
	})
}

//Activity
module.exports.getSpecificTasks = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}


module.exports.getSpecificTaskss = (taskId, newData) => {
	return Task.findById(taskId).then((result, err) => {
		let data = 'completed'
		if(err){
			console.log(err)
			return false
		}	
		result.status = data
		
		return result.save().then((dataSave, err) => {
			if (err){
				console.log(err)
				return false
			} else{
				return dataSave
			}
		})
	})
}