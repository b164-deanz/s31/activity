const express = require("express");
const router = express.Router()

const TaskController = require('../controllers/taskControllers')

//Get all tasks
router.get("/", (req, res) => {
	TaskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})


//Creating Task
router.post("/", (req, res) => {
	TaskController.createTask(req.body).then(result => res.send(result))
})

//Delete Task
router.delete("/:id", (req, res) => {
	TaskController.deleteTask(req.params.id).then(result => res.send(result))
})

// //Update Task
// router.put("/:id", (req, res) => {
// 	TaskController.updateTask(req.params.id, req.body).then(result => res.send(result))
// })


//Activity
router.get("/:id", (req, res) => {
	TaskController.getSpecificTasks(req.params.id).then(resultFromSpecificTask => res.send(resultFromSpecificTask))
})

router.put("/:id/completed", (req, res) => {
	TaskController.getSpecificTaskss(req.params.id).then(resultFromSpecificTask => res.send(resultFromSpecificTask))
})


module.exports = router;